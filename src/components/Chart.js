import React, { useEffect, useRef } from 'react';
import { select, scaleBand, scaleLinear, max } from 'd3';
import useResizeObserver from '../hooks/useResizeObserver';
import { Spin } from 'antd';

function Chart({ data, iteration }) {
  const svgRef = useRef();
  const wrapperRef = useRef();
  const dimensions = useResizeObserver(wrapperRef);

  useEffect(() => {
    const svg = select(svgRef.current);
    if (!dimensions) return;

    // sort data
    data.sort(
      (a, b) =>
        Object.values(b.cases)[iteration] - Object.values(a.cases)[iteration]
    );

    const topData = data.slice(0, 10);

    const yScale = scaleBand()
      .paddingInner(0.1)
      .domain(topData.map((value, index) => index))
      .range([0, dimensions.height]);

    const xScale = scaleLinear()
      .domain([
        0,
        max(topData, (entry) => Object.values(entry.cases)[iteration]),
      ])
      .range([0, dimensions.width]);

    // draw the bars
    svg
      .selectAll('.bar')
      .data(topData, (entry, index) => entry.name)
      .join((enter) =>
        enter.append('rect').attr('y', (entry, index) => yScale(index))
      )
      .attr('fill', (entry) => entry.color)
      .attr('class', 'bar')
      .attr('x', 0)
      .attr('height', yScale.bandwidth())
      .transition()
      .attr('width', (entry) => xScale(Object.values(entry.cases)[iteration]))
      .attr('y', (entry, index) => yScale(index));

    // draw the labels
    svg
      .selectAll('.label')
      .data(topData, (entry, index) => entry.name)
      .join((enter) =>
        enter
          .append('text')
          .attr(
            'y',
            (entry, index) => yScale(index) + yScale.bandwidth() / 2 + 5
          )
      )
      .text(
        (entry) =>
          ` ${entry.name} (${Object.values(entry.cases)[iteration]} cases)`
      )
      .attr('class', 'label')
      .attr('x', 10)
      .transition()
      .attr('y', (entry, index) => yScale(index) + yScale.bandwidth() / 2 + 5);
  }, [data, dimensions, iteration]);

  return (
    <div ref={wrapperRef} style={{ marginBottom: '2rem' }}>
      {!data || data.length < 1 ? (
        <Spin tip="Loading" />
      ) : (
        <svg ref={svgRef}></svg>
      )}
    </div>
  );
}

export default Chart;
