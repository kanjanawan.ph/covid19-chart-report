import React, { useEffect, useState } from 'react';
import useInterval from './hooks/useInterval';
import Chart from './components/Chart';
import './App.css';
import { Button, Space, Typography } from 'antd';

const { Title, Text } = Typography;
const TOTAL_DAY = 1000;

function App() {
  const [data, setData] = useState([]);
  const [dateList, setDateList] = useState([]);
  const [iteration, setIteration] = useState(0);
  const [start, setStart] = useState(false);

  useEffect(() => {
    fetch(`https://disease.sh/v3/covid-19/historical?lastdays=${TOTAL_DAY}`)
      .then((data) => data.json())
      .then((res) => {
        console.log(data);
        const mapData = res.map((item, key) => {
          if (key === 0) {
            const date = Object.keys(item.timeline.cases);
            setDateList(date);
          }
          return {
            name: item?.country,
            cases: item?.timeline?.cases,
            color: `#${Math.floor(Math.random() * 16777215).toString(16)}`,
          };
        });
        setData(mapData);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useInterval(() => {
    if (start && iteration < dateList.length - 1) {
      setIteration(iteration + 1);
    } else {
      setStart(false);
    }
  }, 500);

  return (
    <div className="App">
      <h1>
        <Title underline>COVID-19 GLOBLE CASES</Title>
      </h1>
      <Text mark style={{ textAlign: 'center' }}>
        Date: {dateList[iteration]}
      </Text>
      <Chart data={data} iteration={iteration} />
      <Space wrap>
        <Button type="primary" danger onClick={() => setStart(!start)}>
          {start ? 'Stop' : 'Start'}
        </Button>
        <Button type="primary" onClick={() => setIteration(0)}>
          Reset
        </Button>
      </Space>
      <p>Iteration: {iteration}</p>
    </div>
  );
}

export default App;
